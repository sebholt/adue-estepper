/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#include <adue/pio/leds.hpp>
#include <adue/pmc.hpp>
#include <adue/system_init.hpp>
#include <est/com.hpp>
#include <est/controls.hpp>
#include <est/global.hpp>
#include <est/pwm.hpp>
#include <est/sensors.hpp>
#include <est/steppers.hpp>
#include <est/usb.hpp>
#include <sam3x8e.h>
#include <cstdint>

namespace
{
// -- Variables

bool usb_was_connected;

// -- Functions

void
setup ()
{
  // System setup
  SystemInit ();

  // System ticks
  // Set Systick to 1ms interval, common to all SAM3 variants
  if ( SysTick_Config ( SystemCoreClock / 1000 ) ) {
    // Block
    while ( true )
      ;
  }

  // Watchdog setup
  {
    WDT->WDT_MR = WDT_MR_WDV ( 256 ) | WDT_MR_WDD ( 256 ) | WDT_MR_WDRSTEN |
                  WDT_MR_WDRPROC;
    WDT->WDT_CR = WDT_CR_KEY ( 0xa5 ) | WDT_CR_WDRSTT;
  }

  // Init variables
  usb_was_connected = false;

  // Enable peripheral clocks
  adue::pmc_set_writeprotect ( false );
  // pio
  adue::pmc_enable_periph_clk ( ID_PIOA );
  adue::pmc_enable_periph_clk ( ID_PIOB );
  adue::pmc_enable_periph_clk ( ID_PIOC );
  adue::pmc_enable_periph_clk ( ID_PIOD );
  // timer/counter
  adue::pmc_enable_periph_clk ( ID_TC0 );
  adue::pmc_enable_periph_clk ( ID_TC1 );
  adue::pmc_enable_periph_clk ( ID_TC2 );
  adue::pmc_enable_periph_clk ( ID_TC3 );
  adue::pmc_enable_periph_clk ( ID_TC4 );
  adue::pmc_enable_periph_clk ( ID_TC5 );
  adue::pmc_enable_periph_clk ( ID_TC6 );
  adue::pmc_enable_periph_clk ( ID_TC7 );
  adue::pmc_enable_periph_clk ( ID_TC8 );
  // pwm
  adue::pmc_enable_periph_clk ( ID_PWM );

  // Init pio: leds
  adue::pio::init_leds ();
  adue::pio::set_led_p13 ( false );
  adue::pio::set_led_rx ( false );
  adue::pio::set_led_tx ( false );

  // Init pio: inputs / outputs
  est::global_setup ();
  est::sensors_setup ();
  est::controls_setup ();
  est::pwm_setup ();
  est::steppers_setup ();
  est::com_setup ();
  est::usb_setup ();
}

void
loop ()
{
  est::global_poll ();
  if ( est::usb_bulk_io.is_connnected () ) {
    if ( !usb_was_connected ) {
      usb_was_connected = true;
      adue::pio::set_led_rx ( true );
    }
    est::com_read ();
    est::sensors_poll ();
    {
      est::steppers_controls_interrupt_pause_begin ();
      est::controls_com_poll ();
      est::pwm_com_poll ();
      est::steppers_controls_interrupt_pause_end ();
    }
    est::steppers_poll ();
    est::com_poll ();
    est::com_write ();
  } else {
    if ( usb_was_connected ) {
      usb_was_connected = false;
      adue::pio::set_led_rx ( false );
      est::global_reset ();
    }
  }
}

} // namespace

// -- Main function

int
main ()
{
  // User setup
  setup ();

  // Main loop
  while ( true ) {
    loop ();
  }

  return 0;
}
