/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <adue/array.hpp>
#include <adue/pio/pin.hpp>

namespace est
{

constexpr std::size_t sensors_input_1_num = 12;
constexpr std::size_t sensors_1_num = sensors_input_1_num;

void
sensors_setup ();

void
sensors_reset ();

void
sensors_poll ();

} // namespace est
