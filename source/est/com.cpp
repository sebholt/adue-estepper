/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#include "com.hpp"
#include <adue/serialize.hpp>
#include <est/com/sender/incomplete_message.hpp>
#include <est/com/sender/invalid_control_index.hpp>
#include <est/com/sender/pong.hpp>
#include <est/com/sender/unknown_message_type.hpp>
#include <est/controls.hpp>
#include <est/global.hpp>
#include <est/steppers.hpp>
#include <est/usb.hpp>
#include <estepper/out/messages.hpp>
#include <cstdint>

namespace est
{

namespace
{

// -- Reading
struct Read
{
  static constexpr std::uint_fast32_t buffer_size = 256;
  std::uint8_t buffer[ buffer_size ];
  std::uint8_t * buffer_cur;
  std::uint_fast32_t frame_bytes_missing;
  com::sender::Unknown_Message_Type unknown_message_type_sender;
  com::sender::Incomplete_Message incomplete_message_sender;

  void
  send_incomplete_message ()
  {
    auto & sender = incomplete_message_sender;
    if ( !sender.queued ) {
      sender.set_length_and_type ( buffer[ 0 ], buffer[ 1 ] );
      com_push_sender ( &sender );
    }
  }
} read;

// -- Sending
struct Send
{
  std::uint8_t const * data;
  std::size_t data_size;
  com::Sender * sender;
  struct
  {
    com::Sender * front;
    com::Sender ** last;
  } queue;
} send;

// -- Senders
std::uint32_t _com_ping_count;
com::sender::Pong _com_pong;
com::sender::Invalid_Control_Index _com_invalid_control_1_index;
com::sender::Invalid_Control_Index _com_invalid_control_8_index;
com::sender::Invalid_Control_Index _com_invalid_control_16_index;
com::sender::Invalid_Control_Index _com_invalid_control_32_index;
com::sender::Invalid_Control_Index _com_invalid_control_64_index;

// -- Handlers

void
handler_zero ()
{
  // TODO: Implement
}

void
handler_ping ()
{
  // Request a pong message
  ++_com_ping_count;
  // Rest the reset counter
  global_reset_counter = 0;
}

void
handler_control_1 ()
{
  // Check message length
  if ( read.buffer[ 0 ] != ( 2 + 1 ) ) {
    read.send_incomplete_message ();
    return;
  }
  // Check control index
  if ( read.buffer[ 2 ] < controls_1_num ) {
    steppers_controls_interrupt_pause_begin ();
    set_control_1 ( read.buffer[ 2 ], read.buffer[ 3 ] );
    steppers_controls_interrupt_pause_end ();
  } else {
    // Invalid control index. Send error message.
    auto & sender = _com_invalid_control_1_index;
    if ( !sender.queued ) {
      sender.set_control_index ( read.buffer[ 2 ] );
      com_push_sender ( &sender );
    }
  }
}

void
handler_control_8 ()
{
  // Check message length
  if ( read.buffer[ 0 ] != ( 2 + 1 ) ) {
    read.send_incomplete_message ();
    return;
  }
  // Check control index
  if ( read.buffer[ 2 ] < controls_8_num ) {
    // Set control
    steppers_controls_interrupt_pause_begin ();
    set_control_8 ( read.buffer[ 2 ], read.buffer[ 3 ] );
    steppers_controls_interrupt_pause_end ();
  } else {
    // Invalid control index. Send error message.
    auto & sender = _com_invalid_control_8_index;
    if ( !sender.queued ) {
      sender.set_control_index ( read.buffer[ 2 ] );
      com_push_sender ( &sender );
    }
  }
}

void
handler_control_16 ()
{
  // Check message length
  if ( read.buffer[ 0 ] != ( 2 + 2 ) ) {
    read.send_incomplete_message ();
    return;
  }
  // Invalid control index. Send error message.
  {
    auto & sender = _com_invalid_control_16_index;
    if ( !sender.queued ) {
      sender.set_control_index ( read.buffer[ 2 ] );
      com_push_sender ( &sender );
    }
  }
}

void
handler_control_32 ()
{
  // Check message length
  if ( read.buffer[ 0 ] != ( 2 + 4 ) ) {
    read.send_incomplete_message ();
    return;
  }
  // Check control index
  if ( read.buffer[ 2 ] < controls_32_num ) {
    // Set control
    steppers_controls_interrupt_pause_begin ();
    set_control_32 ( read.buffer[ 2 ],
                     adue::read_uint32_8le ( &read.buffer[ 3 ] ) );
    steppers_controls_interrupt_pause_end ();
  } else {
    // Invalid control index. Send error message.
    auto & sender = _com_invalid_control_32_index;
    if ( !sender.queued ) {
      sender.set_control_index ( read.buffer[ 2 ] );
      com_push_sender ( &sender );
    }
  }
}

void
handler_control_64 ()
{
  // Check message length
  if ( read.buffer[ 0 ] != ( 2 + 8 ) ) {
    read.send_incomplete_message ();
    return;
  }
  // Invalid control index. Send error message.
  {
    auto & sender = _com_invalid_control_64_index;
    if ( !sender.queued ) {
      sender.set_control_index ( read.buffer[ 2 ] );
      com_push_sender ( &sender );
    }
  }
}

void
handler_stepper_enable ()
{
  steppers_enable ( &read.buffer[ 2 ], read.buffer[ 0 ] - 1 );
}

void
handler_stepper_disable ()
{
  steppers_disable ( &read.buffer[ 2 ], read.buffer[ 0 ] - 1 );
}

void
handler_stepper_data ()
{
  if ( read.buffer[ 0 ] < 2 ) {
    read.send_incomplete_message ();
    return;
  }

  steppers_feed_segments (
      read.buffer[ 2 ], &read.buffer[ 3 ], read.buffer[ 0 ] - 2 );
}

// -- Message handlers
using Message_Handler = void ( * ) ();
Message_Handler message_handlers[ estepper::in::message_type_count ]{
    &handler_ping,
    &handler_control_1,
    &handler_control_8,
    &handler_control_16,
    &handler_control_32,
    &handler_control_64,
    &handler_stepper_enable,
    &handler_stepper_disable,
    &handler_stepper_data };

void
read_eval_frame ()
{
  if ( read.buffer[ 0 ] == 0 ) {
    // Zero length message
    handler_zero ();
  } else {
    // Non zero length message
    // The second byte is the type byte
    const std::uint8_t type = read.buffer[ 1 ];
    if ( type < estepper::in::message_type_count ) {
      // Call message handler
      message_handlers[ type ]();
    } else {
      // Send unknown message type
      auto & sender = read.unknown_message_type_sender;
      if ( !sender.queued ) {
        sender.set_length_and_type ( read.buffer[ 0 ], type );
        com_push_sender ( &sender );
      }
    }
  }
}

} // namespace

void
com_setup ()
{
  // Setup message read buffer
  read.buffer_cur = &read.buffer[ 0 ];
  read.frame_bytes_missing = 0;
  read.unknown_message_type_sender.init ();
  read.incomplete_message_sender.init ();

  // Setup message send structures
  send.data = nullptr;
  send.data_size = 0;
  send.sender = nullptr;
  send.queue.front = nullptr;
  send.queue.last = &send.queue.front;

  // Sender
  _com_ping_count = 0;
  _com_pong.init ();
  _com_invalid_control_1_index.init ( 1 );
  _com_invalid_control_8_index.init ( 8 );
  _com_invalid_control_16_index.init ( 16 );
  _com_invalid_control_32_index.init ( 32 );
  _com_invalid_control_64_index.init ( 64 );
}

void
com_reset ()
{
  // Clear current sender
  if ( send.sender != nullptr ) {
    send.data = nullptr;
    send.data_size = 0;
    send.sender->queued = false;
    send.sender = nullptr;
  }

  // Clear send queue
  if ( send.queue.front != nullptr ) {
    while ( send.queue.front != nullptr ) {
      com::Sender * sender = send.queue.front;
      send.queue.front = sender->queue_next;
      // Clear sender
      sender->queue_next = nullptr;
      sender->queued = false;
    }
    // Reset last pointer
    send.queue.last = &send.queue.front;
  }

  _com_ping_count = 0;
}

void
com_read ()
{
  if ( !usb_bulk_io.read_bank_poll () ) {
    return;
  }

  volatile std::uint8_t * read_ptr = usb_bulk_io.read_bank ();
  volatile std::uint8_t * read_ptr_end =
      read_ptr + usb_bulk_io.read_bank_bytes ();
  while ( read_ptr != read_ptr_end ) {
    *read.buffer_cur = *read_ptr;
    ++read_ptr, ++read.buffer_cur;

    if ( read.frame_bytes_missing == 0 ) {
      // First frame byte contains the frame body size
      read.frame_bytes_missing = read.buffer[ 0 ];
    } else {
      // Frame body byte was read
      --read.frame_bytes_missing;
    }

    // Evaluate complete frame
    if ( read.frame_bytes_missing == 0 ) {
      // Evaluate
      read_eval_frame ();
      // Reset read buffer
      read.buffer_cur = &read.buffer[ 0 ];
      read.frame_bytes_missing = 0;
    }
  }

  usb_bulk_io.read_bank_clear ();
}

void
com_poll ()
{
  if ( _com_ping_count != 0 ) {
    auto & sender = _com_pong;
    if ( !sender.queued ) {
      com_push_sender ( &sender );
      --_com_ping_count;
    }
  }
}

void
com_write ()
{
  bool data_written = false;
  while ( true ) {
    // Load next sender
    if ( send.sender == nullptr ) {
      if ( send.queue.front == nullptr ) {
        // No senders available
        break;
      }
      // Pop sender from queue
      send.sender = send.queue.front;
      send.queue.front = send.sender->queue_next;
      // Check if this was the last sender
      if ( send.queue.front == nullptr ) {
        send.queue.last = &send.queue.front;
      }
      send.sender->queue_next = nullptr;
      // Update send data reference
      send.data = send.sender->data;
      send.data_size = send.sender->data_size;
    }

    // Write data of the current sender
    if ( send.sender != nullptr ) {
      if ( send.data_size != 0 ) {
        if ( !usb_bulk_io.write_bank_available () ) {
          // Bank not available
          break;
        }
        std::size_t size_written =
            usb_bulk_io.write_some ( send.data, send.data_size, false );
        if ( size_written == 0 ) {
          // No data written
          break;
        }
        // Update remaining data reference
        send.data += size_written;
        send.data_size -= size_written;
        data_written = true;
      }

      if ( send.data_size == 0 ) {
        send.data = nullptr;
        // Release sender
        send.sender->queued = false;
        send.sender = nullptr;
      }
    }
  }

  if ( data_written ) {
    usb_bulk_io.write_flush_on_demand ();
  }
}

void
com_push_sender ( com::Sender * sender_n )
{
  *send.queue.last = sender_n;
  send.queue.last = &sender_n->queue_next;
  sender_n->queued = true;
}

} // namespace est
