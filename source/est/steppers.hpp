/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <cstddef>
#include <cstdint>

namespace est
{

constexpr std::size_t steppers_num_motor = 4;
constexpr std::size_t steppers_num_controls = 1;
constexpr std::size_t steppers_num = steppers_num_motor + steppers_num_controls;

void
steppers_setup ();

void
steppers_reset ();

void
steppers_poll ();

void
steppers_controls_interrupt_pause_begin ();

void
steppers_controls_interrupt_pause_end ();

void
steppers_enable ( std::uint8_t const * indices_n, std::size_t size_n );

void
steppers_disable ( std::uint8_t const * indices_n, std::size_t size_n );

void
steppers_feed_segments ( std::uint8_t stepper_index_n,
                         std::uint8_t const * segments_n,
                         std::size_t segments_size_n );

} // namespace est
