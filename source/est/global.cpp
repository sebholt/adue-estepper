/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#include "global.hpp"
#include <adue/pio/leds.hpp>
#include <est/com.hpp>
#include <est/controls.hpp>
#include <est/pwm.hpp>
#include <est/sensors.hpp>
#include <est/steppers.hpp>

namespace est
{

namespace
{
struct
{
  volatile bool flag;
  std::uint_fast32_t counter;
} blink;

volatile bool global_reset_flag;
} // namespace

volatile std::uint32_t global_reset_counter;

void
global_setup ()
{
  blink.flag = false;
  blink.counter = 0;

  global_reset_flag = false;
  global_reset_counter = 0;
}

void
global_reset ()
{
  com_reset ();
  steppers_reset ();
  pwm_reset ();
  controls_reset ();
  sensors_reset ();
}

void
global_poll ()
{
  // Reset watchdog
  WDT->WDT_CR = WDT_CR_KEY ( 0xa5 ) | WDT_CR_WDRSTT;

  // Blink
  if ( blink.flag ) {
    blink.flag = false;
    adue::pio::toggle_led_p13 ();
  }

  // Reset
  if ( global_reset_flag ) {
    global_reset_flag = false;
    global_reset ();
  }
}

} // namespace est

// -- Interrupt handlers

void
SysTick_Handler ()
{
  ++est::blink.counter;
  if ( est::blink.counter == 500 ) {
    est::blink.counter = 0;
    est::blink.flag = true;
  }
  ++est::global_reset_counter;
  if ( est::global_reset_counter == 250 ) {
    est::global_reset_counter = 0;
    est::global_reset_flag = true;
  }
}
