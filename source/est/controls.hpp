/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <est/pwm.hpp>
#include <cstddef>

namespace est
{

constexpr std::size_t controls_output_1_num = 12;
constexpr std::size_t controls_1_num = controls_output_1_num + pwm_num;
constexpr std::size_t controls_8_num = pwm_num;
constexpr std::size_t controls_16_num = 0;
constexpr std::size_t controls_32_num = pwm_num;
constexpr std::size_t controls_64_num = 0;

void
controls_setup ();

void
controls_reset ();

void
controls_com_poll ();

void
set_control_1 ( std::size_t index_n, bool state_n );

void
set_control_8 ( std::size_t index_n, std::uint8_t state_n );

void
set_control_16 ( std::size_t index_n, std::uint16_t state_n );

void
set_control_32 ( std::size_t index_n, std::uint32_t state_n );

void
set_control_64 ( std::size_t index_n, std::uint64_t state_n );

} // namespace est
