/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <adue/array.hpp>
#include <adue/pio/pin.hpp>
#include <cstddef>

namespace est
{

constexpr std::size_t pwm_num = 4;

void
pwm_setup ();

void
pwm_reset ();

void
pwm_com_poll ();

// -- Run control

void
pwm_set_enabled ( std::size_t index_n, bool flag_n );

// -- Setup

void
pwm_clock_select ( std::size_t index_n, std::size_t clock_n );

void
pwm_set_duty_period ( std::size_t index_n,
                      std::uint16_t duty_n,
                      std::uint16_t period_n );

} // namespace est
