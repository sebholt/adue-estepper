/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#include "stepper_controls.hpp"
#include <est/controls.hpp>
#include <estepper/segment/types.hpp>

namespace est
{

void
Stepper_Controls::init ( std::uint8_t index_n )
{
  Stepper::init ( index_n );
  // Timer counter
  _tc_channel.interrupt_enable_rc ();
  // Abstract interface
  _load_first_step = &Stepper_Controls::load_first_step_anon;
  // Segment setup
  _segment.count = 0;
}

void
Stepper_Controls::reset ()
{
  Stepper::reset ();
}

bool
Stepper_Controls::load_first_step_anon ( Stepper & stepper_n )
{
  return static_cast< Stepper_Controls & > ( stepper_n )
      .load_first_step_private ();
}

bool
Stepper_Controls::load_first_step_private ()
{
  bool step_loaded = false;
  bool bad_segment = false;
  estepper::in::Bad_Seg_Type bad_segment_type = estepper::in::BAD_SEG_UNKNOWN;
  std::uint8_t bad_segment_byte = 0;

  do {
    // Load first segment
    if ( !_queue.is_empty () ) {
      std::uint16_t const word16 = queue_read_16 ();
      std::uint8_t const byte1 =
          reinterpret_cast< std::uint8_t const * > ( &word16 )[ 0 ];
      std::uint8_t const byte2 =
          reinterpret_cast< std::uint8_t const * > ( &word16 )[ 1 ];
      switch ( byte1 ) {
      case estepper::segment::CTL_SLEEP:
        _segment.count = byte2;
        break;
      case estepper::segment::CTL_1:
        if ( _queue.size () >= 1 ) {
          set_control_1 ( byte2, queue_read_16 () );
        } else {
          // Missing word
          bad_segment = true;
          bad_segment_type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
        }
        break;
      case estepper::segment::CTL_8:
        if ( _queue.size () >= 1 ) {
          set_control_8 ( byte2, queue_read_16 () );
        } else {
          // Missing word
          bad_segment = true;
          bad_segment_type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
        }
        break;
      case estepper::segment::CTL_16:
        if ( _queue.size () >= 1 ) {
          set_control_16 ( byte2, queue_read_16 () );
        } else {
          // Missing word
          bad_segment = true;
          bad_segment_type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
        }
        break;
      case estepper::segment::CTL_32:
        if ( _queue.size () >= 2 ) {
          set_control_32 ( byte2, queue_read_32 () );
        } else {
          // Missing words
          bad_segment = true;
          bad_segment_type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
        }
        break;
      case estepper::segment::CTL_64:
        if ( _queue.size () >= 4 ) {
          set_control_64 ( byte2, queue_read_64 () );
        } else {
          // Missing words
          bad_segment = true;
          bad_segment_type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
        }
        break;
      default:
        // Bad type
        bad_segment = true;
        bad_segment_type = estepper::in::BAD_SEG_BAD_TYPE;
        bad_segment_byte = byte1;
        break;
      }
      if ( bad_segment ) {
        break;
      }
    } else {
      // Empty queue
      break;
    }

    // Load first step
    if ( _segment.count != 0 ) {
      if ( !_queue.is_empty () ) {
        // Update timer match register from queue
        _tc_channel.rc_set ( queue_read_16 () );
        --_segment.count;
        _running = true;
        step_loaded = true;
      } else {
        // Bad segment. Missing data.
        _segment.count = 0;
        bad_segment = true;
        bad_segment_type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
      }
      break;
    }

  } while ( true );

  if ( bad_segment ) {
    _bad_segment_async.flag = 1;
    _bad_segment_async.type = bad_segment_type;
    _bad_segment_async.byte = bad_segment_byte;
    // Clear queue
    queue_drop_all ();
  }

  return step_loaded;
}

void
Stepper_Controls::interrupt ()
{
  std::uint32_t const status = _tc_channel.status ();

  if ( ( status & TC_SR_CPCS ) != 0 ) {
    do {
      // Load next segment on demand
      if ( _segment.count == 0 ) {
        if ( !_queue.is_empty () ) {
          bool bad_segment = false;
          estepper::in::Bad_Seg_Type bad_segment_type =
              estepper::in::BAD_SEG_UNKNOWN;
          std::uint8_t bad_segment_byte = 0;
          std::uint16_t const word16 = queue_read_16 ();
          std::uint8_t const byte1 =
              reinterpret_cast< std::uint8_t const * > ( &word16 )[ 0 ];
          std::uint8_t const byte2 =
              reinterpret_cast< std::uint8_t const * > ( &word16 )[ 1 ];
          switch ( byte1 ) {
          case estepper::segment::CTL_SLEEP:
            _segment.count = byte2;
            break;
          case estepper::segment::CTL_1:
            if ( _queue.size () >= 1 ) {
              set_control_1 ( byte2, queue_read_16 () );
            } else {
              // Missing word
              bad_segment = true;
              bad_segment_type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
            }
            break;
          case estepper::segment::CTL_8:
            if ( _queue.size () >= 1 ) {
              set_control_8 ( byte2, queue_read_16 () );
            } else {
              // Missing word
              bad_segment = true;
              bad_segment_type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
            }
            break;
          case estepper::segment::CTL_16:
            if ( _queue.size () >= 1 ) {
              set_control_16 ( byte2, queue_read_16 () );
            } else {
              // Missing word
              bad_segment = true;
              bad_segment_type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
            }
            break;
          case estepper::segment::CTL_32:
            if ( _queue.size () >= 2 ) {
              set_control_32 ( byte2, queue_read_32 () );
            } else {
              // Missing words
              bad_segment = true;
              bad_segment_type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
            }
            break;
          case estepper::segment::CTL_64:
            if ( _queue.size () >= 4 ) {
              set_control_64 ( byte2, queue_read_64 () );
            } else {
              // Missing words
              bad_segment = true;
              bad_segment_type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
            }
            break;
          default:
            // Bad type
            bad_segment = true;
            bad_segment_type = estepper::in::BAD_SEG_BAD_TYPE;
            bad_segment_byte = byte1;
            break;
          }
          if ( bad_segment ) {
            _bad_segment_async.flag = 1;
            _bad_segment_async.type = bad_segment_type;
            _bad_segment_async.byte = bad_segment_byte;
            // Clear queue
            queue_drop_all ();
            // Stop the clock
            _tc_channel.clock_disable ();
            _stopped_async = 1;
            break;
          }
        } else {
          // Queue empty. Regular stop.
          _tc_channel.clock_disable ();
          _stopped_async = 1;
          break;
        }
      }

      // Load next step
      if ( _segment.count != 0 ) {
        if ( !_queue.is_empty () ) {
          // Update timer match register from queue
          _tc_channel.rc_set ( queue_read_16 () );
          --_segment.count;
        } else {
          // Bad segment. Missing data. Clear queue and stop.
          _segment.count = 0;
          _bad_segment_async.flag = 1;
          _bad_segment_async.type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
          // Clear queue
          queue_drop_all ();
          // Stop the clock
          _tc_channel.clock_disable ();
          _stopped_async = 1;
        }
        break;
      }
    } while ( true );
  }
}

} // namespace est
