/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <est/stepper/stepper.hpp>

namespace est
{

/// @brief Stepper that executes control changes
///
class Stepper_Controls : public Stepper
{
  public:
  // -- Init and reset

  void
  init ( std::uint8_t index_n );

  void
  reset ();

  // -- Session

  static bool
  load_first_step_anon ( Stepper & stepper_n );

  bool
  load_first_step_private ();

  void
  interrupt ();

  private:
  // -- Attributes
  struct
  {
    std::size_t count;
  } _segment;
};

} // namespace est
