/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#include "stepper_motor.hpp"
#include <estepper/segment/types.hpp>

namespace est
{

void
Stepper_Motor::init ( std::uint8_t index_n,
                      const adue::pio::Pin_Output & pin_dir_n,
                      const adue::pio::Pin_Output & pin_step_n )
{
  Stepper::init ( index_n );
  // Timer counter
  _tc_channel.interrupt_enable_ra ();
  _tc_channel.interrupt_enable_rc ();
  // Abstract interface
  _load_first_step = &Stepper_Motor::load_first_step_anon;
  // Segment setup
  _segment.count = 0;
  _segment.sleep = false;
  // Direction pin setup
  _pin_dir.reset ( pin_dir_n.pio (), pin_dir_n.mask () );
  _pin_dir.setup ();
  // Step pin setup
  _pin_step.reset ( pin_step_n.pio (), pin_step_n.mask () );
  _pin_step.setup ();
}

void
Stepper_Motor::reset ()
{
  Stepper::reset ();
  // Clear segment
  _segment.count = 0;
  _segment.sleep = false;
  // Clear output pin states
  _pin_dir.set_low ();
  _pin_step.set_low ();
}

bool
Stepper_Motor::load_first_step_anon ( Stepper & stepper_n )
{
  return static_cast< Stepper_Motor & > ( stepper_n )
      .load_first_step_private ();
}

bool
Stepper_Motor::load_first_step_private ()
{
  bool step_loaded = false;
  bool bad_segment = false;
  estepper::in::Bad_Seg_Type bad_segment_type = estepper::in::BAD_SEG_UNKNOWN;
  std::uint8_t bad_segment_byte = 0;

  do {
    // Load first segment
    if ( _queue.size () >= 1 ) {
      std::uint16_t const word16 = queue_read_16 ();
      std::uint8_t const type =
          reinterpret_cast< std::uint8_t const * > ( &word16 )[ 0 ];
      std::uint8_t const count =
          reinterpret_cast< std::uint8_t const * > ( &word16 )[ 1 ];
      if ( type == estepper::segment::MOTOR_SLEEP ) {
        _segment.count = count;
        _segment.sleep = true;
      } else if ( type == estepper::segment::MOTOR_FORWARD ) {
        _segment.count = count;
        _segment.sleep = false;
        // Set direction pin
        _pin_dir.set_low ();
      } else if ( type == estepper::segment::MOTOR_BACKWARD ) {
        _segment.count = count;
        _segment.sleep = false;
        // Set direction pin
        _pin_dir.set_high ();
      } else {
        // Bad segment.
        bad_segment = true;
        bad_segment_type = estepper::in::BAD_SEG_BAD_TYPE;
        bad_segment_byte = type;
        break;
      }
    } else {
      // Empty queue
      break;
    }

    // Load first step
    if ( _segment.count != 0 ) {
      if ( !_queue.is_empty () ) {
        // Set step pin
        if ( !_segment.sleep ) {
          _pin_step.set_high ();
        }
        // Update timer match register from queue
        std::uint32_t const step_ticks = queue_read_16 ();
        _tc_channel.ra_set ( step_ticks / 2 );
        _tc_channel.rc_set ( step_ticks );
        --_segment.count;
        _running = true;
        step_loaded = true;
      } else {
        // Bad segment. Missing data.
        _segment.count = 0;
        bad_segment = true;
        bad_segment_type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
      }
      break;
    }

  } while ( true );

  if ( bad_segment ) {
    _bad_segment_async.flag = 1;
    _bad_segment_async.type = bad_segment_type;
    _bad_segment_async.byte = bad_segment_byte;
    // Clear queue
    queue_drop_all ();
    // Clear pin states
    _pin_dir.set_low ();
    _pin_step.set_low ();
  }

  return step_loaded;
}

void
Stepper_Motor::interrupt ()
{
  std::uint32_t const status = _tc_channel.status ();
  if ( ( status & TC_SR_CPAS ) != 0 ) {
    // Set step pin to low
    if ( !_segment.sleep ) {
      _pin_step.set_low ();
    }
  }
  if ( ( status & TC_SR_CPCS ) != 0 ) {
    do {
      // Load next segment on demand
      if ( _segment.count == 0 ) {
        if ( !_queue.is_empty () ) {
          std::uint16_t const word16 = queue_read_16 ();
          std::uint8_t const type =
              reinterpret_cast< std::uint8_t const * > ( &word16 )[ 0 ];
          std::uint8_t const count =
              reinterpret_cast< std::uint8_t const * > ( &word16 )[ 1 ];
          if ( type == estepper::segment::MOTOR_SLEEP ) {
            _segment.count = count;
            _segment.sleep = true;
          } else if ( type == estepper::segment::MOTOR_FORWARD ) {
            _segment.count = count;
            _segment.sleep = false;
            // Set direction pin
            _pin_dir.set_low ();
          } else if ( type == estepper::segment::MOTOR_BACKWARD ) {
            _segment.count = count;
            _segment.sleep = false;
            // Set direction pin
            _pin_dir.set_high ();
          } else {
            // Bad segment
            _bad_segment_async.flag = 1;
            _bad_segment_async.type = estepper::in::BAD_SEG_BAD_TYPE;
            _bad_segment_async.byte = type;
            // Clear queue
            queue_drop_all ();
            // Stop the clock
            _tc_channel.clock_disable ();
            _stopped_async = 1;
            break;
          }
        } else {
          // Queue empty. Regular stop.
          _tc_channel.clock_disable ();
          _stopped_async = 1;
          break;
        }
      }

      // Load next step
      if ( _segment.count != 0 ) {
        if ( !_queue.is_empty () ) {
          // Set step pin
          if ( !_segment.sleep ) {
            _pin_step.set_high ();
          }
          // Update timer match register from queue
          std::uint32_t const step_ticks = queue_read_16 ();
          _tc_channel.ra_set ( step_ticks / 2 );
          _tc_channel.rc_set ( step_ticks );
          --_segment.count;
        } else {
          // Bad segment. Missing data. Clear queue and stop.
          _segment.count = 0;
          _bad_segment_async.flag = 1;
          _bad_segment_async.type = estepper::in::BAD_SEG_MISSING_DATA_WORD;
          // Clear queue
          queue_drop_all ();
          // Stop the clock
          _tc_channel.clock_disable ();
          _stopped_async = 1;
        }
        break;
      }
    } while ( true );
  }
}

} // namespace est
