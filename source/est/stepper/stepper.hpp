/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <adue/ring_buffer.hpp>
#include <adue/tc/channel.hpp>
#include <est/com/sender/stepper_bad_segment.hpp>
#include <est/com/sender/stepper_buffer_overflow.hpp>
#include <est/com/sender/stepper_disabled.hpp>
#include <est/com/sender/stepper_words_processed.hpp>

namespace est
{

/// @brief Stepper base class
///
class Stepper
{
  public:
  using Load_First_Step_Func = bool ( * ) ( Stepper & stepper_n );

  union Bad_Segment
  {
    std::uint32_t word;
    struct
    {
      std::uint8_t flag;
      std::uint8_t type;
      std::uint8_t byte;
    };
  };

  void
  init ( std::uint8_t index_n );

  std::uint8_t
  index () const
  {
    return _index;
  }

  bool
  enabled () const
  {
    return _enabled;
  }

  void
  reset ();

  // -- Starting

  bool
  load_first_step ()
  {
    return _load_first_step ( *this );
  }

  void
  timer_start ()
  {
    _tc_channel.clock_enable_start ();
  }

  // -- Enable / disable

  void
  enable ();

  void
  disable ();

  // -- Interrupt pause

  void
  interrupt_pause_begin ();

  void
  interrupt_pause_end ();

  // -- Polling

  void
  poll ();

  // -- Queue feeding

  void
  feed_queue ( std::uint8_t const * segments_n, std::size_t size_n );

  protected:
  std::uint16_t
  queue_read_16 ();

  std::uint32_t
  queue_read_32 ();

  std::uint64_t
  queue_read_64 ();

  void
  queue_drop_all ();

  protected:
  std::uint8_t _index;
  bool _enabled;
  bool _running;
  Bad_Segment _bad_segment;
  bool _disabled_message;
  volatile Bad_Segment _bad_segment_async;
  volatile std::uint32_t _words_processed_async;
  std::uint32_t _words_processed;
  volatile std::uint32_t _stopped_async;
  adue::tc::Channel _tc_channel;
  adue::Ring_Buffer< std::uint16_t, 4096 > _queue;
  Load_First_Step_Func _load_first_step;
  // -- Com senders
  com::sender::Stepper_Buffer_Overflow _com_stepper_buffer_overflow;
  com::sender::Stepper_Bad_Segment _com_stepper_bad_segment;
  com::sender::Stepper_Words_Processed _com_stepper_words_processed;
  com::sender::Stepper_Disabled _com_stepper_disabled;
};

} // namespace est
