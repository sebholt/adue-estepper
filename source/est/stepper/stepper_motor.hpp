/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <adue/pio/pin.hpp>
#include <est/stepper/stepper.hpp>
#include <cstddef>

namespace est
{

/// @brief Stepper that generates a PWM signal for stepper motor control
///
class Stepper_Motor : public Stepper
{
  public:
  // -- Init and reset

  void
  init ( std::uint8_t index_n,
         const adue::pio::Pin_Output & pin_dir_n,
         const adue::pio::Pin_Output & pin_step_n );

  void
  reset ();

  // -- Session

  static bool
  load_first_step_anon ( Stepper & stepper_n );

  bool
  load_first_step_private ();

  void
  interrupt ();

  private:
  // -- Attributes
  struct
  {
    std::size_t count;
    bool sleep;
  } _segment;
  adue::pio::Pin_Output _pin_dir;
  adue::pio::Pin_Output _pin_step;
};

} // namespace est
