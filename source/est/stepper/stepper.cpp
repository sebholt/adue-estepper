/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#include "stepper.hpp"
#include <est/com.hpp>

namespace est
{

void
Stepper::init ( std::uint8_t index_n )
{
  static_assert ( sizeof ( _bad_segment ) == sizeof ( std::uint32_t ) );
  _index = index_n;
  _enabled = false;
  _running = false;
  _bad_segment.word = 0;
  _disabled_message = false;
  _bad_segment_async.word = 0;
  _words_processed_async = 0;
  _words_processed = 0;
  _stopped_async = 0;

  // Timer counter setup
  {
    _tc_channel.init ( index_n );
    // Disable TC clock / Stop
    _tc_channel.clock_disable ();
    // Disable interrupts
    _tc_channel.interrupt_disable ( 0xFFFFFFFF );
    // Clear status register
    _tc_channel.status ();
    // Initialize match registers
    _tc_channel.ra_set ( 0xFFFFFFFF );
    _tc_channel.rb_set ( 0xFFFFFFFF );
    _tc_channel.rc_set ( 0xFFFFFFFF );
    // Set mode
    _tc_channel.set_mode ( TC_CMR_WAVE | TC_CMR_WAVSEL_UP_RC |
                           TC_CMR_TCCLKS_TIMER_CLOCK1 );

    // Enable interrupt controller interrupts
    NVIC_EnableIRQ ( _tc_channel.irqn () );
  }

  _queue.reset ();
  _load_first_step = nullptr;
  _com_stepper_buffer_overflow.init ( index_n );
  _com_stepper_bad_segment.init ( index_n );
  _com_stepper_words_processed.init ( index_n );
  _com_stepper_disabled.init ( index_n );
}

void
Stepper::reset ()
{
  // Disable TC clock / Stop
  _tc_channel.clock_disable ();
  // Clear status register
  _tc_channel.status ();

  _enabled = false;
  _running = false;
  _bad_segment.word = 0;
  _disabled_message = false;
  _bad_segment_async.word = 0;
  _words_processed_async = 0;
  _words_processed = 0;
  _stopped_async = 0;

  _queue.reset ();
}

void
Stepper::enable ()
{
  _enabled = true;
}

void
Stepper::disable ()
{
  if ( !_enabled ) {
    return;
  }
  _enabled = false;
  if ( !_running ) {
    _disabled_message = true;
  }
}

void
Stepper::interrupt_pause_begin ()
{
  NVIC_DisableIRQ ( _tc_channel.irqn () );
}

void
Stepper::interrupt_pause_end ()
{
  NVIC_EnableIRQ ( _tc_channel.irqn () );
}

void
Stepper::poll ()
{
  std::uint32_t stopped = 0;

  // Read volatile flags
  {
    interrupt_pause_begin ();
    // Bad segment
    _bad_segment.word = _bad_segment_async.word;
    _bad_segment_async.word = 0;
    // Processed bytes
    _words_processed += _words_processed_async;
    _words_processed_async = 0;
    // Stopped
    stopped = _stopped_async;
    _stopped_async = 0;
    interrupt_pause_end ();
  }

  // Send bad segment
  if ( _bad_segment.flag != 0 ) {
    auto & sender = _com_stepper_bad_segment;
    if ( !sender.queued ) {
      sender.set_type ( _bad_segment.type );
      sender.set_byte ( _bad_segment.byte );
      com_push_sender ( &sender );
      _bad_segment.word = 0;
    }
  }

  // Send processed bytes
  if ( _words_processed != 0 ) {
    auto & sender = _com_stepper_words_processed;
    if ( !sender.queued ) {
      {
        std::uint16_t const words_16 = _words_processed;
        _words_processed -= words_16;
        sender.set_word_count ( words_16 );
      }
      com_push_sender ( &sender );
    }
  }

  // Handle a stepper stop
  if ( stopped != 0 ) {
    _running = false;
    if ( !_enabled ) {
      _disabled_message = true;
    }
  }

  // Send disabled message
  if ( _disabled_message && ( _words_processed == 0 ) ) {
    auto & sender = _com_stepper_disabled;
    if ( !sender.queued ) {
      com_push_sender ( &sender );
      _disabled_message = false;
    }
  }

  // Restart enabled stepper with non empty queue
  if ( _enabled && !_running && !_queue.is_empty () ) {
    if ( load_first_step () ) {
      timer_start ();
    }
  }
}

void
Stepper::feed_queue ( std::uint8_t const * segments_n, std::size_t size_n )
{
  interrupt_pause_begin ();
  static_assert ( sizeof ( std::uint16_t ) == 2 );
  std::size_t size16 = size_n / sizeof ( std::uint16_t );
  if ( size16 <= _queue.size_free () ) {
    // Write segements to ring buffer
    _queue.write_back_unchecked (
        reinterpret_cast< std::uint16_t const * > ( segments_n ), size16 );
  } else {
    // Buffer overflow. Send error message.
    auto & sender = _com_stepper_buffer_overflow;
    if ( !sender.queued ) {
      com_push_sender ( &sender );
    }
  }
  interrupt_pause_end ();
}

std::uint16_t
Stepper::queue_read_16 ()
{
  std::uint16_t res = _queue.front ();
  _queue.pop_front_not_empty ();
  _words_processed_async += 1;
  return res;
}

std::uint32_t
Stepper::queue_read_32 ()
{
  std::uint16_t const v0 = _queue.item ( 0 );
  std::uint16_t const v1 = _queue.item ( 1 );
  _queue.pop_front_unchecked ( 2 );
  _words_processed_async += 2;
  return ( static_cast< std::uint32_t > ( v1 ) << 16 |
           static_cast< std::uint32_t > ( v0 ) );
}

std::uint64_t
Stepper::queue_read_64 ()
{
  std::uint16_t const v0 = _queue.item ( 0 );
  std::uint16_t const v1 = _queue.item ( 1 );
  std::uint16_t const v2 = _queue.item ( 2 );
  std::uint16_t const v3 = _queue.item ( 3 );
  _queue.pop_front_unchecked ( 4 );
  _words_processed_async += 4;
  return ( static_cast< std::uint64_t > ( v3 ) << 48 |
           static_cast< std::uint64_t > ( v2 ) << 32 |
           static_cast< std::uint64_t > ( v1 ) << 16 |
           static_cast< std::uint64_t > ( v0 ) );
}

void
Stepper::queue_drop_all ()
{
  _words_processed_async += _queue.size ();
  _queue.clear ();
}

} // namespace est
