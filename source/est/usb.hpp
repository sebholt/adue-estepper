/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <adue/usb/usb_bulk_io.hpp>

namespace est
{

// USB instance
extern adue::USB_Bulk_IO usb_bulk_io;

void
usb_setup ();

} // namespace est
