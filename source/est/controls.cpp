/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#include "controls.hpp"
#include <adue/array.hpp>
#include <adue/pio/pin.hpp>
#include <est/com.hpp>
#include <est/com/sender/control_1.hpp>

namespace est
{

namespace
{

adue::Array< adue::pio::Pin_Output, controls_output_1_num > output_pins{ {
    //
    { PIOB, 1 << 26 },
    { PIOA, 1 << 14 },
    { PIOA, 1 << 15 },
    { PIOD, 1 << 0 },
    { PIOD, 1 << 1 },
    { PIOD, 1 << 2 },
    { PIOD, 1 << 3 },
    { PIOD, 1 << 6 },
    { PIOD, 1 << 9 },
    { PIOA, 1 << 7 },
    { PIOD, 1 << 10 },
    { PIOC, 1 << 1 }
    //
} };

adue::Array< bool, controls_output_1_num > com_control_1_changed;
adue::Array< est::com::sender::Control_1, controls_output_1_num > com_sender_1;

} // namespace

void
controls_setup ()
{
  for ( auto & pin : output_pins ) {
    pin.setup ();
  }
  com_control_1_changed.fill ( false );
  for ( std::size_t ii = 0; ii != controls_output_1_num; ++ii ) {
    com_sender_1[ ii ].init ( ii );
  }
}

void
controls_reset ()
{
  for ( auto & pin : output_pins ) {
    pin.set ( false );
  }
  com_control_1_changed.fill ( false );
}

void
controls_com_poll ()
{
  // Enqueue state senders on demand
  for ( std::size_t ii = 0; ii != controls_output_1_num; ++ii ) {
    auto & changed = com_control_1_changed[ ii ];
    if ( !changed ) {
      continue;
    }
    auto & sender = com_sender_1[ ii ];
    if ( sender.queued ) {
      continue;
    }
    // Submit sender
    sender.set_state ( output_pins[ ii ].is_high () );
    com_push_sender ( &sender );
    changed = false;
  }
}

void
set_control_1 ( std::size_t index_n, bool state_n )
{
  // Set output
  if ( index_n < controls_output_1_num ) {
    com_control_1_changed[ index_n ] = true;
    output_pins[ index_n ].set ( state_n );
    return;
  }

  // Start/stop PWM
  const std::size_t pwm_index = index_n - controls_output_1_num;
  if ( pwm_index < pwm_num ) {
    pwm_set_enabled ( pwm_index, state_n );
    return;
  }
}

void
set_control_8 ( std::size_t index_n, std::uint8_t state_n )
{
  // Select PWM clock
  pwm_clock_select ( index_n, state_n );
}

void
set_control_16 ( std::size_t index_n [[maybe_unused]],
                 std::uint16_t state_n [[maybe_unused]] )
{
}

void
set_control_32 ( std::size_t index_n, std::uint32_t state_n )
{
  // Setup PWM duty cycle
  pwm_set_duty_period (
      index_n, ( state_n & 0xffff ), ( ( state_n >> 16 ) & 0xffff ) );
}

void
set_control_64 ( std::size_t index_n [[maybe_unused]],
                 std::uint64_t state_n [[maybe_unused]] )
{
}

} // namespace est
