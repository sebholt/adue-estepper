/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#include "pwm.hpp"
#include <adue/array.hpp>
#include <adue/pio/pin.hpp>
#include <adue/pwm/channel.hpp>
#include <est/com.hpp>
#include <est/com/sender/control_1.hpp>
#include <est/com/sender/control_32.hpp>
#include <est/com/sender/control_8.hpp>
#include <est/controls.hpp>

namespace est
{

namespace
{
// Clock bits
constexpr std::size_t num_clocks = 11;
adue::Array< std::uint32_t, num_clocks > pwm_clock_bits = { {
    PWM_CMR_CPRE_MCK,
    PWM_CMR_CPRE_MCK_DIV_2,
    PWM_CMR_CPRE_MCK_DIV_4,
    PWM_CMR_CPRE_MCK_DIV_8,
    PWM_CMR_CPRE_MCK_DIV_16,
    PWM_CMR_CPRE_MCK_DIV_32,
    PWM_CMR_CPRE_MCK_DIV_64,
    PWM_CMR_CPRE_MCK_DIV_128,
    PWM_CMR_CPRE_MCK_DIV_256,
    PWM_CMR_CPRE_MCK_DIV_512,
    PWM_CMR_CPRE_MCK_DIV_1024,
} };

// PWM channel
struct PWM_Channel
{
  bool running;
  std::uint8_t clock;
  std::uint16_t duty;
  std::uint16_t period;
  adue::pwm::Channel pwm;
};
adue::Array< PWM_Channel, pwm_num > pwm_channels;

// Com
adue::Array< bool, pwm_num > com_control_1_changed;
adue::Array< bool, pwm_num > com_control_8_changed;
adue::Array< bool, pwm_num > com_control_32_changed;
adue::Array< est::com::sender::Control_1, pwm_num > com_sender_1;
adue::Array< est::com::sender::Control_8, pwm_num > com_sender_8;
adue::Array< est::com::sender::Control_32, pwm_num > com_sender_32;

inline bool
pwm_index_valid ( std::size_t index_n )
{
  return ( index_n < pwm_num );
}

inline bool
clock_index_valid ( std::size_t index_n )
{
  return ( index_n < num_clocks );
}

void
pwm_setup_mode ( PWM_Channel & channel_n )
{
  const std::uint32_t clock_bits = pwm_clock_bits[ channel_n.clock ];
  channel_n.pwm.set_mode ( clock_bits );
}

} // namespace

void
pwm_setup ()
{
  // Initialize channels
  {
    auto init_channel = [] ( PWM_Channel & channel_n, std::uint8_t index_n ) {
      channel_n.running = false;
      channel_n.clock = 0;
      channel_n.duty = 0x0000;
      channel_n.period = 0xffff;
      channel_n.pwm.init ( index_n );
      pwm_setup_mode ( channel_n );
      channel_n.pwm.set_duty ( channel_n.duty );
      channel_n.pwm.set_period ( channel_n.period );
    };

    init_channel ( pwm_channels[ 0 ], 7 );
    init_channel ( pwm_channels[ 1 ], 6 );
    init_channel ( pwm_channels[ 2 ], 5 );
    init_channel ( pwm_channels[ 3 ], 4 );
  }

  // Setup output pins
  {
    struct PWM_Pin_Setup
    {
      adue::pio::Pin_Output pio_pin;
      bool peripheral_b;
    };
    adue::Array< PWM_Pin_Setup, 4 > pwm_pins = { {
        { { PIOC, 1 << 24 }, true }, //
        { { PIOC, 1 << 23 }, true }, //
        { { PIOC, 1 << 22 }, true }, //
        { { PIOC, 1 << 21 }, true }  //
    } };
    for ( auto const & item : pwm_pins ) {
      if ( !item.peripheral_b ) {
        item.pio_pin.setup_peripheral_a ();
      } else {
        item.pio_pin.setup_peripheral_b ();
      }
    }
  }

  // Setup com senders
  com_control_1_changed.fill ( false );
  com_control_8_changed.fill ( false );
  com_control_32_changed.fill ( false );
  for ( std::size_t ii = 0; ii != pwm_num; ++ii ) {
    com_sender_1[ ii ].init ( controls_output_1_num + ii );
  }
  for ( std::size_t ii = 0; ii != pwm_num; ++ii ) {
    com_sender_8[ ii ].init ( ii );
  }
  for ( std::size_t ii = 0; ii != pwm_num; ++ii ) {
    com_sender_32[ ii ].init ( ii );
  }
}

void
pwm_reset ()
{
  for ( std::size_t ii = 0; ii != pwm_num; ++ii ) {
    pwm_set_enabled ( ii, false );
    pwm_clock_select ( ii, 0 );
    pwm_set_duty_period ( ii, 0x0000, 0xffff );
  }
  com_control_1_changed.fill ( false );
  com_control_8_changed.fill ( false );
  com_control_32_changed.fill ( false );
}

void
pwm_com_poll ()
{
  // Enqueue running state senders on demand
  for ( std::size_t ii = 0; ii != pwm_num; ++ii ) {
    auto & changed = com_control_1_changed[ ii ];
    if ( !changed ) {
      continue;
    }
    auto & sender = com_sender_1[ ii ];
    if ( sender.queued ) {
      continue;
    }
    // Submit sender
    sender.set_state ( pwm_channels[ ii ].running );
    com_push_sender ( &sender );
    changed = false;
  }

  // Enqueue clock selection senders on demand
  for ( std::size_t ii = 0; ii != pwm_num; ++ii ) {
    auto & changed = com_control_8_changed[ ii ];
    if ( !changed ) {
      continue;
    }
    auto & sender = com_sender_8[ ii ];
    if ( sender.queued ) {
      continue;
    }
    // Submit sender
    sender.set_state ( pwm_channels[ ii ].clock );
    com_push_sender ( &sender );
    changed = false;
  }

  // Enqueue duty and period senders on demand
  for ( std::size_t ii = 0; ii != pwm_num; ++ii ) {
    auto & changed = com_control_32_changed[ ii ];
    if ( !changed ) {
      continue;
    }
    auto & sender = com_sender_32[ ii ];
    if ( sender.queued ) {
      continue;
    }
    // Submit sender
    auto & channel = pwm_channels[ ii ];
    sender.set_state ( ( std::uint32_t ( channel.period ) << 16 ) &
                       channel.duty );
    com_push_sender ( &sender );
    changed = false;
  }
}

void
pwm_set_enabled ( std::size_t index_n, bool flag_n )
{
  if ( !pwm_index_valid ( index_n ) ) {
    return;
  }
  auto & channel = pwm_channels[ index_n ];
  if ( channel.running == flag_n ) {
    return;
  }
  // Change PWM channel run state
  channel.running = flag_n;
  if ( flag_n ) {
    channel.pwm.enable ();
  } else {
    channel.pwm.disable ();
  }
  // Request com message
  com_control_1_changed[ index_n ] = true;
}

void
pwm_clock_select ( std::size_t index_n, std::size_t clock_n )
{
  if ( !pwm_index_valid ( index_n ) ) {
    return;
  }
  if ( !clock_index_valid ( clock_n ) ) {
    return;
  }
  auto & channel = pwm_channels[ index_n ];
  if ( channel.clock == clock_n ) {
    return;
  }
  // Disable PWM
  pwm_set_enabled ( index_n, false );
  // Select new clock
  channel.clock = clock_n;
  // Update channel mode
  pwm_setup_mode ( channel );
  // Request com message
  com_control_8_changed[ index_n ] = true;
}

void
pwm_set_duty_period ( std::size_t index_n,
                      std::uint16_t duty_n,
                      std::uint16_t period_n )
{
  if ( !pwm_index_valid ( index_n ) ) {
    return;
  }
  auto & channel = pwm_channels[ index_n ];
  if ( ( channel.duty == duty_n ) && ( channel.period == period_n ) ) {
    return;
  }
  // Select duty values
  channel.duty = duty_n;
  channel.period = period_n;
  if ( channel.running ) {
    // Use update registers
    channel.pwm.set_duty_update ( duty_n );
    channel.pwm.set_period_update ( period_n );
  } else {
    // Use direct registers
    channel.pwm.set_duty ( duty_n );
    channel.pwm.set_period ( period_n );
  }
  // Request com message
  com_control_32_changed[ index_n ] = true;
}

} // namespace est
