/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <cstdint>

namespace est
{

extern volatile std::uint32_t global_reset_counter;

void
global_setup ();

void
global_reset ();

void
global_poll ();

} // namespace est
