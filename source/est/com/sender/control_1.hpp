/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <est/com/sender.hpp>
#include <estepper/in/messages.hpp>
#include <cstdint>

namespace est::com::sender
{

class Control_1 : public Sender
{
  public:
  void
  init ( std::uint8_t control_index_n )
  {
    Sender::init ( buffer, sizeof ( buffer ) );
    buffer[ 0 ] = sizeof ( buffer ) - 1;
    buffer[ 1 ] = estepper::in::CONTROL_1;
    buffer[ 2 ] = control_index_n;
    buffer[ 3 ] = 0x00; // Some initial default value
  }

  void
  set_state ( bool state_n )
  {
    buffer[ 3 ] = state_n ? 0xff : 0x00;
  }

  std::uint8_t buffer[ 4 ];
};

} // namespace est::com::sender
