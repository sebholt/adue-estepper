/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <est/com/sender.hpp>
#include <estepper/in/messages.hpp>
#include <cstdint>

namespace est::com::sender
{

class Invalid_Control_Index : public Sender
{
  public:
  void
  init ( std::uint8_t control_bits_n )
  {
    Sender::init ( buffer, sizeof ( buffer ) );
    buffer[ 0 ] = sizeof ( buffer ) - 1;
    buffer[ 1 ] = estepper::in::INVALID_STEPPER_INDEX;
    buffer[ 2 ] = control_bits_n;
    buffer[ 3 ] = 0xff; // Some default invalid index
  }

  void
  set_control_index ( std::uint8_t control_index_n )
  {
    buffer[ 3 ] = control_index_n;
  }

  std::uint8_t buffer[ 4 ];
};

} // namespace est::com::sender
