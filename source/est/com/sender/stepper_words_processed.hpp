/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <adue/serialize.hpp>
#include <est/com/sender.hpp>
#include <estepper/in/messages.hpp>
#include <cstdint>

namespace est::com::sender
{

class Stepper_Words_Processed : public Sender
{
  public:
  void
  init ( std::uint8_t stepper_index_n )
  {
    Sender::init ( buffer, sizeof ( buffer ) );
    buffer[ 0 ] = sizeof ( buffer ) - 1;
    buffer[ 1 ] = estepper::in::STEPPER_WORDS_PROCESSED;
    buffer[ 2 ] = stepper_index_n;
    // Some initial default value
    buffer[ 3 ] = 0;
    buffer[ 4 ] = 0;
  }

  void
  set_word_count ( std::uint16_t words_n )
  {
    adue::set_8le_uint16 ( &buffer[ 3 ], words_n );
  }

  std::uint8_t buffer[ 5 ];
};

} // namespace est::com::sender
