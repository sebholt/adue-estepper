/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <est/com/sender.hpp>
#include <estepper/in/messages.hpp>
#include <cstdint>

namespace est::com::sender
{

class Stepper_Bad_Segment : public Sender
{
  public:
  void
  init ( std::uint8_t stepper_index_n )
  {
    Sender::init ( buffer, sizeof ( buffer ) );
    buffer[ 0 ] = sizeof ( buffer ) - 1;
    buffer[ 1 ] = estepper::in::STEPPER_BAD_SEGMENT;
    buffer[ 2 ] = stepper_index_n;
    buffer[ 3 ] = estepper::in::BAD_SEG_UNKNOWN;
    buffer[ 4 ] = 0;
  }

  void
  set_type ( std::uint8_t type_n )
  {
    buffer[ 3 ] = type_n;
  }

  void
  set_byte ( std::uint8_t byte_n )
  {
    buffer[ 4 ] = byte_n;
  }

  std::uint8_t buffer[ 5 ];
};

} // namespace est::com::sender
