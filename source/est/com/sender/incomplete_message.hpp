/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <est/com/sender.hpp>
#include <estepper/in/messages.hpp>
#include <cstdint>

namespace est::com::sender
{

class Incomplete_Message : public Sender
{
  public:
  void
  init ()
  {
    Sender::init ( buffer, sizeof ( buffer ) );
    buffer[ 0 ] = sizeof ( buffer ) - 1;
    buffer[ 1 ] = estepper::in::INCOMPLETE_MESSAGE;
    // Some initial default value
    buffer[ 2 ] = 0xff;
    buffer[ 3 ] = 0xff;
  }

  void
  set_length_and_type ( std::uint8_t length_n, std::uint8_t type_n )
  {
    buffer[ 2 ] = length_n;
    buffer[ 3 ] = type_n;
  }

  std::uint8_t buffer[ 4 ];
};

} // namespace est::com::sender
