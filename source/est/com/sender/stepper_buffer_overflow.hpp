/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <est/com/sender.hpp>
#include <estepper/in/messages.hpp>
#include <cstdint>

namespace est::com::sender
{

class Stepper_Buffer_Overflow : public Sender
{
  public:
  void
  init ( std::uint8_t stepper_index_n )
  {
    Sender::init ( buffer, sizeof ( buffer ) );
    buffer[ 0 ] = sizeof ( buffer ) - 1;
    buffer[ 1 ] = estepper::in::STEPPER_BUFFER_OVERFLOW;
    buffer[ 2 ] = stepper_index_n;
  }

  std::uint8_t buffer[ 3 ];
};

} // namespace est::com::sender
