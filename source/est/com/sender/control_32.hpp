/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <adue/serialize.hpp>
#include <est/com/sender.hpp>
#include <estepper/in/messages.hpp>
#include <cstdint>

namespace est::com::sender
{

class Control_32 : public Sender
{
  public:
  void
  init ( std::uint8_t control_index_n )
  {
    Sender::init ( buffer, sizeof ( buffer ) );
    buffer[ 0 ] = sizeof ( buffer ) - 1;
    buffer[ 1 ] = estepper::in::CONTROL_32;
    buffer[ 2 ] = control_index_n;
    // Some initial default value
    buffer[ 3 ] = 0x00;
    buffer[ 4 ] = 0x00;
    buffer[ 5 ] = 0x00;
    buffer[ 6 ] = 0x00;
  }

  void
  set_state ( std::uint32_t state_n )
  {
    adue::set_8le_uint32 ( &buffer[ 3 ], state_n );
  }

  std::uint8_t buffer[ 7 ];
};

} // namespace est::com::sender
