/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <est/com/sender.hpp>
#include <estepper/in/messages.hpp>
#include <cstdint>

namespace est::com::sender
{

class Invalid_Stepper_Index : public Sender
{
  public:
  void
  init ()
  {
    Sender::init ( buffer, sizeof ( buffer ) );
    buffer[ 0 ] = sizeof ( buffer ) - 1;
    buffer[ 1 ] = estepper::in::INVALID_STEPPER_INDEX;
    buffer[ 2 ] = 0xff; // Some default invalid index
  }

  void
  set_stepper_index ( std::uint8_t stepper_index_n )
  {
    buffer[ 2 ] = stepper_index_n;
  }

  std::uint8_t buffer[ 3 ];
};

} // namespace est::com::sender
