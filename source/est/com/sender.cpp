/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#include "sender.hpp"

namespace est::com
{

void
Sender::init ( std::uint8_t const * data_n, std::size_t data_size_n )
{
  data = data_n;
  data_size = data_size_n;
  queued = false;
  queue_next = nullptr;
}

} // namespace est::com
