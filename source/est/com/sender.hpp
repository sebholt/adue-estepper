/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <cstddef>
#include <cstdint>

namespace est::com
{

class Sender
{
  public:
  void
  init ( std::uint8_t const * data_n, std::size_t data_size_n );

  std::uint8_t const * data;
  std::size_t data_size;
  bool queued;
  Sender * queue_next;
};

} // namespace est::com
