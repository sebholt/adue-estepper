/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#include "steppers.hpp"
#include <adue/array.hpp>
#include <adue/pio/pin.hpp>
#include <est/com.hpp>
#include <est/com/sender/invalid_stepper_index.hpp>
#include <est/stepper/stepper_controls.hpp>
#include <est/stepper/stepper_motor.hpp>

namespace est
{

namespace
{
adue::Array< Stepper_Motor, steppers_num_motor > steppers_motor;
adue::Array< Stepper_Controls, steppers_num_controls > steppers_controls;
adue::Array< Stepper *, steppers_num > steppers;

com::sender::Invalid_Stepper_Index _com_invalid_stepper_index;
} // namespace

void
steppers_setup ()
{
  // Initialize steppers
  {
    adue::Array< adue::pio::Pin_Output, steppers_num_motor * 2 > output_pins{ {
        //
        { PIOC, 1 << 3 },
        { PIOC, 1 << 2 },

        { PIOC, 1 << 5 },
        { PIOC, 1 << 4 },

        { PIOC, 1 << 7 },
        { PIOC, 1 << 6 },

        { PIOC, 1 << 9 },
        { PIOC, 1 << 8 }
        //
    } };

    std::size_t index = 0;
    // PWM steppers
    for ( auto & stepper : steppers_motor ) {
      stepper.init (
          index, output_pins[ index * 2 ], output_pins[ index * 2 + 1 ] );
      steppers[ index ] = &stepper;
      ++index;
    }
    // Controls steppers
    for ( auto & stepper : steppers_controls ) {
      stepper.init ( index );
      steppers[ index ] = &stepper;
      ++index;
    }
  }

  _com_invalid_stepper_index.init ();
}

void
steppers_reset ()
{
  for ( auto & stepper : steppers_motor ) {
    stepper.reset ();
  }
  for ( auto & stepper : steppers_controls ) {
    stepper.reset ();
  }
}

void
steppers_poll ()
{
  for ( auto & stepper : steppers ) {
    stepper->poll ();
  }
}

void
steppers_controls_interrupt_pause_begin ()
{
  for ( auto & stepper : steppers_controls ) {
    stepper.interrupt_pause_begin ();
  }
}

void
steppers_controls_interrupt_pause_end ()
{
  for ( auto & stepper : steppers_controls ) {
    stepper.interrupt_pause_end ();
  }
}

void
steppers_enable ( std::uint8_t const * indices_n, std::size_t size_n )
{
  std::size_t enabled_steppers_num = 0;
  adue::Array< Stepper *, steppers_num > enabled_steppers;

  for ( std::size_t ii = 0; ii != size_n; ++ii ) {
    std::uint8_t stepper_index = indices_n[ ii ];
    if ( stepper_index < steppers_num ) {
      // Enable stepper on demand
      Stepper * stepper = steppers[ stepper_index ];
      if ( !stepper->enabled () ) {
        stepper->enable ();
        enabled_steppers[ enabled_steppers_num ] = stepper;
        ++enabled_steppers_num;
      }
    } else {
      // Invalid stepper index. Send error message.
      auto & sender = _com_invalid_stepper_index;
      if ( !sender.queued ) {
        sender.set_stepper_index ( stepper_index );
        com_push_sender ( &sender );
      }
    }
  }

  // Load first steps and store startable steppers
  std::size_t start_num = 0;
  adue::Array< Stepper *, steppers_num > startables;
  for ( std::size_t ii = 0; ii != enabled_steppers_num; ++ii ) {
    Stepper * stepper = enabled_steppers[ ii ];
    if ( stepper->load_first_step () ) {
      startables[ start_num ] = stepper;
      ++start_num;
    }
  }

  // Start timers as close to each other as possible
  for ( std::size_t ii = 0; ii != start_num; ++ii ) {
    startables[ ii ]->timer_start ();
  }
}

void
steppers_disable ( std::uint8_t const * indices_n, std::size_t size_n )
{
  for ( std::size_t ii = 0; ii != size_n; ++ii ) {
    std::uint8_t stepper_index = indices_n[ ii ];
    if ( stepper_index < steppers_num ) {
      // Disable stepper
      steppers[ stepper_index ]->disable ();
    } else {
      // Invalid stepper index. Send error message.
      auto & sender = _com_invalid_stepper_index;
      if ( !sender.queued ) {
        sender.set_stepper_index ( stepper_index );
        com_push_sender ( &sender );
      }
    }
  }
}

void
steppers_feed_segments ( std::uint8_t stepper_index_n,
                         std::uint8_t const * segments_n,
                         std::size_t segments_size_n )
{
  if ( stepper_index_n < steppers_num ) {
    // Feed stepper
    steppers[ stepper_index_n ]->feed_queue ( segments_n, segments_size_n );
  } else {
    // Invalid stepper index. Send error message.
    auto & sender = _com_invalid_stepper_index;
    if ( !sender.queued ) {
      sender.set_stepper_index ( stepper_index_n );
      com_push_sender ( &sender );
    }
  }
}

} // namespace est

// -- Timer handlers

void
TC0_Handler ()
{
  est::steppers_motor[ 0 ].interrupt ();
}

void
TC1_Handler ()
{
  est::steppers_motor[ 1 ].interrupt ();
}

void
TC2_Handler ()
{
  est::steppers_motor[ 2 ].interrupt ();
}

void
TC3_Handler ()
{
  est::steppers_motor[ 3 ].interrupt ();
}

void
TC4_Handler ()
{
  est::steppers_controls[ 0 ].interrupt ();
}
