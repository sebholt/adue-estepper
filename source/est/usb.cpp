/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#include "usb.hpp"
#include <cstdint>

namespace est
{

// USB instance
adue::USB_Bulk_IO usb_bulk_io;

namespace
{
// USB descriptor strings
static const std::uint8_t usb_desc_str_manu[] = {
    'S', 0, 'T', 0, 'e', 0, 'c', 0, 'h', 0 };
static const std::uint8_t usb_desc_str_product[] = {
    'S', 0, 't', 0, 'e', 0, 'p', 0, 'p', 0, 'e', 0, 'r', 0, //
    '-', 0, '4', 0, ':', 0, '1', 0 };
static const std::uint8_t usb_desc_str_serial[] = {
    '0', 0, '0', 0, '.', 0, '1', 0, '0', 0 };
} // namespace

void
usb_setup ()
{
  // USB init & enable
  usb_bulk_io.init ();
  usb_bulk_io.set_desc_string ( adue::USB_Bulk_IO::SI_MANUFACTURER,
                                usb_desc_str_manu,
                                sizeof ( usb_desc_str_manu ) );
  usb_bulk_io.set_desc_string ( adue::USB_Bulk_IO::SI_PRODUCT,
                                usb_desc_str_product,
                                sizeof ( usb_desc_str_product ) );
  usb_bulk_io.set_desc_string ( adue::USB_Bulk_IO::SI_SERIAL,
                                usb_desc_str_serial,
                                sizeof ( usb_desc_str_serial ) );

  usb_bulk_io.enable ();
}

} // namespace est
