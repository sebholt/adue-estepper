/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#include "sensors.hpp"
#include <est/com.hpp>
#include <est/com/sender/sensor_1.hpp>

namespace est
{

namespace
{
adue::Array< adue::pio::Pin_Input, sensors_input_1_num > input_pins_1{ {
    //
    { PIOB, 1 << 21 },
    { PIOB, 1 << 14 },
    { PIOC, 1 << 13 },
    { PIOC, 1 << 12 },
    { PIOC, 1 << 15 },
    { PIOC, 1 << 14 },
    { PIOC, 1 << 17 },
    { PIOC, 1 << 16 },
    { PIOC, 1 << 19 },
    { PIOC, 1 << 18 },
    { PIOA, 1 << 19 },
    { PIOA, 1 << 20 }
    //
} };

adue::Array< bool, sensors_input_1_num > input_states_emitted_1;
adue::Array< com::sender::Sensor_1, sensors_input_1_num > message_sender_1;

} // namespace

void
sensors_setup ()
{
  for ( auto & pin : input_pins_1 ) {
    pin.setup ();
  }
  for ( auto & state : input_states_emitted_1 ) {
    state = false;
  }
  for ( std::size_t ii = 0; ii != sensors_input_1_num; ++ii ) {
    message_sender_1[ ii ].init ( ii );
  }
}

void
sensors_reset ()
{
  for ( auto & state : input_states_emitted_1 ) {
    state = false;
  }
}

void
sensors_poll ()
{
  for ( std::size_t ii = 0; ii != sensors_input_1_num; ++ii ) {
    bool state = input_pins_1[ ii ].read ();
    bool & estate = input_states_emitted_1[ ii ];
    if ( state == estate ) {
      continue;
    }
    auto & sender = message_sender_1[ ii ];
    if ( sender.queued ) {
      continue;
    }
    estate = state;
    sender.set_state ( state );
    com_push_sender ( &sender );
  }
}

} // namespace est
