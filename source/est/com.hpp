/// adue_estepper: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_estepper.txt file

#pragma once

#include <est/com/sender.hpp>

namespace est
{

void
com_setup ();

void
com_reset ();

void
com_read ();

void
com_poll ();

void
com_write ();

void
com_push_sender ( com::Sender * sender_n );

} // namespace est
